This is just a simple mod that removes 5 health from you about every minute and 30 seconds, 5 minutes after the last time you've ate. 
The way you replenish your health is by eating like normal.

How to install:
First install .NET Scripthook, then copy the Hunger.net.dll and Hunger.ini files to the scripts folder in your GTA IV directory.

Controls:
Right-CTRL + H = To toggle hunger (hunger is enabled on startup)
-Keys are changeable in the ini file

Credits:
RiceBox: for requesting the script
LetsPlayOrDy (now named Jitnaught): for creating the script

How to compile:
I lost the project files after a hard drive failure, so you'll need to:

* create a new C# library/DLL project in Visual Studio
* replace the generated cs file with the cs file in this archive
* download .NET ScriptHook and add a reference to it
* click compile
* change the extension of the generated .dll file to .net.dll
