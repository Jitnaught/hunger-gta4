﻿using GTA;
using System;
using System.IO;
using System.Windows.Forms;

namespace Hunger
{
    public class Hunger : Script
    {
        bool hungerEnabled = true, useHoldKey;
        Keys pressKey, holdKey;
        int timeBetweenEachHealthRemoval, timeAfterEatingToStartGettingHungry, healthRemovalAmount;

        public Hunger()
        {
            if (!File.Exists(Settings.Filename))
            {
                Settings.SetValue("hunger_toggle_press_key", "keys", Keys.H);
                Settings.SetValue("hunger_toggle_hold_key", "keys", Keys.RControlKey);
                Settings.SetValue("use_hunger_toggle_hold_key", "keys", true);
                Settings.SetValue("time_between_each_health_removal", "advanced", 100000);
                Settings.SetValue("time_after_eating_to_start_getting_hungry", "advanced", 300000);
                Settings.SetValue("health_removal_amount", "advanced", 5);
                Settings.Save();
            }

            if (!Game.isMultiplayer)
            {
                pressKey = Settings.GetValueKey("hunger_toggle_press_key", "keys", Keys.H);
                holdKey = Settings.GetValueKey("hunger_toggle_hold_key", "keys", Keys.RControlKey);
                useHoldKey = Settings.GetValueBool("use_hunger_toggle_hold_key", "keys", true);
                timeBetweenEachHealthRemoval = Settings.GetValueInteger("time_between_each_health_removal", "advanced", 100000);
                timeAfterEatingToStartGettingHungry = Settings.GetValueInteger("time_after_eating_to_start_getting_hungry", "advanced", 300000);
                healthRemovalAmount = Settings.GetValueInteger("health_removal_amount", "advanced", 5);

                KeyDown += Hunger_KeyDown;

                new GTA.Timer(timeBetweenEachHealthRemoval, true).Tick += Hunger_Tick; //20000 -> 100000 now
                new GTA.Timer(100, true).Tick += LastTimeAte_Tick;
            }
        }

        void Hunger_KeyDown(object sender, GTA.KeyEventArgs e)
        {
            if ((!useHoldKey || isKeyPressed(holdKey)) && e.Key == pressKey)
            {
                hungerEnabled = !hungerEnabled;
                Game.DisplayText("Hunger has been " + (hungerEnabled ? "enabled" : "disabled"), 2000);
            }
        }

        int lastTimeAte = Game.GameTime;
        int burgersEaten = Game.GetIntegerStatistic(IntegerStatistic.BURGERS_EATEN), hotdogsEaten = Game.GetIntegerStatistic(IntegerStatistic.HOTDOGS_EATEN), mealsEaten = Game.GetIntegerStatistic(IntegerStatistic.MEALS_EATEN), nutsEaten = Game.GetIntegerStatistic(IntegerStatistic.NUTS_EATEN), sodasDrunk = Game.GetIntegerStatistic(IntegerStatistic.SODA_DRUNK);
        private void LastTimeAte_Tick(object sender, EventArgs e)
        {
            if (hungerEnabled && Player.Character.isAlive && !GTA.Native.Function.Call<bool>("GET_MISSION_FLAG"))
            {

                int newburgersEaten = Game.GetIntegerStatistic(IntegerStatistic.BURGERS_EATEN), newhotdogsEaten = Game.GetIntegerStatistic(IntegerStatistic.HOTDOGS_EATEN), newmealsEaten = Game.GetIntegerStatistic(IntegerStatistic.MEALS_EATEN), newnutsEaten = Game.GetIntegerStatistic(IntegerStatistic.NUTS_EATEN), newsodasDrunk = Game.GetIntegerStatistic(IntegerStatistic.SODA_DRUNK);

                if (newburgersEaten > burgersEaten || newhotdogsEaten > hotdogsEaten || newmealsEaten > mealsEaten || newnutsEaten > nutsEaten || newsodasDrunk > sodasDrunk)
                {
                    lastTimeAte = Game.GameTime;
                    burgersEaten = newburgersEaten;
                    hotdogsEaten = newhotdogsEaten;
                    mealsEaten = newmealsEaten;
                    nutsEaten = newnutsEaten;
                    sodasDrunk = newsodasDrunk;
                }
            }
        }

        private void Hunger_Tick(object sender, EventArgs e)
        {
            if (hungerEnabled && Player.Character.isAlive && !GTA.Native.Function.Call<bool>("GET_MISSION_FLAG") && Math.Abs(Game.GameTime - lastTimeAte) > timeAfterEatingToStartGettingHungry)
            {
                if (Player.Character.Health > healthRemovalAmount) Player.Character.Health -= healthRemovalAmount; 
                else Player.Character.Die();
            }
        }
    }
}
